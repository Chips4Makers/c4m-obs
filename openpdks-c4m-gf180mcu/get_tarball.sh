#!/bin/sh
. ./version.sh

zip=open_pdk.zip
curl --output ${zip} \
     --location "https://gitlab.com/api/v4/projects/Chips4Makers%2Fc4m-pdk-gf180mcu/jobs/artifacts/v${version}/download?job=release"
unzip ${zip}
rm -f ${zip}

mv open_pdk ${osc_package}
tar czvf ${osc_package}.tgz ${osc_package}
rm -fr ${osc_package}

ls -l ${osc_package}.tgz

