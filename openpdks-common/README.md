Support scripts for OBS openpdks-common package building; script:
* `update_obs.sh`: Update the files for OBS; checkout if not done yet
* `version.sh`: file with version information. Needs to be updated to
    switch to other version of the PDK.
The tarball `openpdk-common.tgz` is just an empty tarball.

