osc_project="home:chips4makers:dev"
osc_package=ngspice-vamodels

ngspice_version=43
ngspice_commit=2af390f0b12ec460f29464d7325cf3ab5b02d98b
vamodels_version=2.1
vamodels_location="https://github.com/dwarning/VA-Models/releases/download/v${vamodels_version}"
vamodels_dir=VA-Models-osdilibs
version=${ngspice_version}+VAM${vamodels_version}

builddeps=", libx11-dev, libxaw7-dev, libreadline-dev, bison, flex"
