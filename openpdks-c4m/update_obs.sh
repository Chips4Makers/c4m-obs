#!/bin/sh
. ./version.sh

osc_pkgdir=${osc_project}/${osc_package}
if [ ! -d ${osc_pkgdir} ]; then
  echo "Checking out OBS package"
  osc checkout ${osc_project} ${osc_package}
else
  echo "Updating OBS package"
  osc update ${osc_pkgdir}
fi

cp debian.compat debian.rules ${osc_package}.tgz \
   ${osc_pkgdir}
in_files="debian.changelog debian.control"
in_files="${in_files} ${osc_package}.dsc ${osc_package}.spec PKGBUILD"
for f in ${in_files}; do
  sed \
    -e "s/%VERSION%/${version}/g" \
    -e "s/%DATE%/`date -R`/g" \
    -e "s/%PACKAGE%/${osc_package}/g" \
    < ${f}.in > ${osc_pkgdir}/${f}
done
osc status ${osc_pkgdir}

echo "\nPlease verify ${osc_pkgdir} and commit manually"

