#!/bin/sh
. ./config.sh

d=ngspice-ngspice-$commit
zip=$d.zip
curl --output ${zip} \
     --location "https://sourceforge.net/code-snapshots/git/n/ng/ngspice/ngspice.git/$zip"
unzip ${zip}
rm -f ${zip}

mv $d ${osc_package}
tar czvf ${osc_package}.tgz ${osc_package}
rm -fr ${osc_package}

ls -l ${osc_package}.tgz
