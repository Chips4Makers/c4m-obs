osc_project="home:chips4makers:dev"
osc_package=magic

version=8.3.492
debversion=${version}-c4m.0
repo=https://gitlab.com/Chips4Makers/magic
tag=${version}-obs
email="Chips4Makers <info@chips4makers.io>"

builddeps=", python3, tcsh, tcl-dev, tk-dev, libx11-dev, libcairo2-dev"
