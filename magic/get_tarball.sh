#!/bin/sh
. ./config.sh

# Clone repo if it does not exist
if [ ! -d ${osc_package} ]; then
  git clone $repo.git
fi

# Generate tarball
cd ${osc_package}
git fetch --tags origin
git fetch --all
git archive --prefix=${osc_package}/ -o ../${osc_package}.tar $tag
cd ..
rm -f ${osc_package}.tar.xz
xz ${osc_package}.tar

