#!/bin/sh
PDK_ROOT=/usr/share/open_pdks

echo "installing sky130 pdks"
volare enable --pdk sky130 $1

echo "installing gf180mcu pdks"
volare enable --pdk gf180mcu $1

# Rearranging directories
rm -f /usr/share/open_pdks/sky130* /usr/share/open_pdks/gf180mcu*
mv /usr/share/open_pdks/volare/sky130/versions/$1/* /usr/share/open_pdks/
mv /usr/share/open_pdks/volare/gf180mcu/versions/$1/* /usr/share/open_pdks/
# To save space remove the DRC/LVS testing infrastructure
rm -fr /usr/share/open_pdks/*/libs.tech/klayout/*/testing
rm -fr /usr/share/open_pdks/volare

