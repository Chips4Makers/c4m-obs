#!/bin/sh
. ./version.sh

cont=`docker container run -d chips4makers/build tail -f /dev/null`

docker container cp install_pdks.sh ${cont}:/
docker container exec ${cont} /install_pdks.sh ${pdk_commit}

echo "Generating tarball"
docker container cp ${cont}:/usr/share/open_pdks - | gzip > openpdks-efabless.tgz

echo "Cleaning up"
docker container stop ${cont}
docker container rm ${cont}

