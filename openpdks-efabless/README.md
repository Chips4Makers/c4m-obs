Support scripts for OBS openpdks-efabless package building; script:
* generate_tarball.sh: run docker to generate a new tarball for the PDKs
* update_obs.sg: Update the files for OBS; checkout if not done yet
* version.sh: file with version information. Needs to be updated to
    switch to other version of the PDK.

