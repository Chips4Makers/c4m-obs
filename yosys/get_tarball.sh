#!/bin/sh
. ./config.sh

# Clone repo if it does not exist
if [ ! -d ${osc_package} ]; then
  git clone --recursive $repo.git
fi

# Generate tarballs
cd ${osc_package}
git fetch --all --tags
git archive --prefix=${osc_package}/ -o ../${osc_package}.tar $tag
git checkout -f $tag
git submodule update abc
cd abc
git archive --prefix=abc/ -o ../../abc.tar HEAD
cd ../..

rm -f ${osc_package}.tar.xz
mkdir tar
cd tar
tar xf ../${osc_package}.tar
cd ${osc_package}
tar xf ../../abc.tar
cd ..
echo "Packing tarball"
tar cJf ../${osc_package}.tar.xz ${osc_package}
cd ..
rm -fr tar ${osc_package}.tar abc.tar

ls -l ${osc_package}.tar.xz

