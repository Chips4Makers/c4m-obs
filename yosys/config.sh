osc_project="home:chips4makers:dev"
osc_package=yosys

version=0.45+139
debversion=${version}-c4m.0
repo=https://github.com/YosysHQ/yosys
tag=4d581a9
email="Chips4Makers <info@chips4makers.io>"

builddeps=", python3, pkg-config, flex, bison, libffi-dev, tcl-dev, libreadline-dev"
